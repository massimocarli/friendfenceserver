var pg = require('pg');
var connectionString = process.env.DATABASE_URL 
var client;
var query;

client = new pg.Client(connectionString);
client.connect();


query = client.query("DROP TABLE IF EXISTS registration;");
query = client.query("DROP SEQUENCE IF EXISTS registration_id_seq");

// The table for places (cities)
query = client.query("CREATE SEQUENCE registration_id_seq;");
var createBusStopCache = "CREATE TABLE IF NOT EXISTS registration (  \
    request_id           integer PRIMARY KEY NOT NULL DEFAULT nextval('registration_id_seq'), \
    registration_id      varchar(200), \
    device_id            varchar(60), \
    username             varchar(40), \
    updated_at    timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP);";
query = client.query(createBusStopCache);

query.on('end', function() { client.end(); });

