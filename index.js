var express = require('express')
var app = express();
var pg = require('pg');
var bodyParser = require('body-parser')
var request = require('request');

var jsonParser = bodyParser.json()


app.set('port', (process.env.PORT || 5000))
app.use(express.static(__dirname + '/public'))
app.use(bodyParser.json())

app.get('/', function(request, response) {
  response.send('FriendFence is working!')
})

function sendOkResponse(code, message, res) {
  res.send(code, message);
}

function sendErrorResponse(httpCode, code, message, res) {
  var returnJson = {"code":code, "message":message};
  res.json(httpCode, returnJson);
}

client = new pg.Client(process.env.DATABASE_URL);
client.connect();


app.post('/register', jsonParser, function (request, response) {
  var inputObj = request.body;
  var registrationId = inputObj.registrationId;
  var deviceId = inputObj.deviceId;
  var username = inputObj.username;
  if (!username){
      response.send('{username} is mandatory!' + inputObj)
      return;
  }
  if (!registrationId) {
      response.send('{registrationId} is mandatory!')
      return;
  }  
  if (!deviceId) {
      response.send('{deviceId} is mandatory!')
      return;
  }   
  // We delete previous one
  client.query('DELETE FROM registration WHERE username = $1 AND device_id = $2',
    [username, deviceId], function(err, result) {
      if (err) {
        sendErrorResponse(500, err.code, err.message, response);
      } else {
        // In every case we insert the new data
        client.query('INSERT INTO registration (registration_id, username, device_id) VALUES ($1, $2, $3)',
          [registrationId, username,  deviceId], function(err, result){
            if (err) {
              sendErrorResponse(500, err.code, err.message, response);
            } else {
              // We return the response
              response.json({});
            }
        });
      }
  });
})

app.get('/users', function (request, response) {
  client.query('SELECT * FROM registration', function(err, result) {
    if (err) {
      sendErrorResponse(500, err.code, err.message, response);
    } else { 
      response.send(result.rows);
    }
  });
});


app.post('/send', function (req, res) {
  var inputObj = req.body;
  var to = inputObj.to;
  var senderUser = inputObj.senderUser;  
  var msgBody = inputObj.msgBody;
  if (!to){
      res.send('{to} is mandatory!');
      return;
  }
  if (!senderUser){
      res.send('{senderUser} is mandatory!');
      return;
  }  
  if (!msgBody) {
      res.send('{msgBody} is mandatory!');
      return;
  }  
  var destinations = [];
  // we make a query to detect the user to send the message to
  client.query('SELECT * FROM registration WHERE username = $1', [to], function(err, result) {
    if (err) {
      sendErrorResponse(500, err.code, err.message, res);
    } else { 
      // We create an array with all the registration ids
      if (result.rows && result.rows.length > 0) {
        for (var i = 0; i < result.rows.length; i++){
          var regId = result.rows[i].registration_id;
          destinations.push(regId);
        }
      }
      // If we have destinations we invoke the send otherwise we return an empty message
      if (destinations.length > 0){
          // Create the options
          var gcmOptions = {
            method: 'POST',
            url: 'https://android.googleapis.com/gcm/send',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': 'key=AIzaSyDFr1doZ2C7Uk3dfMEH2Y8Amm2SBZIvAzU'
            },
            json:{
              "registration_ids" : destinations,
              "data" : {
                "msgBody":msgBody,
                "senderUser":senderUser
              }
            }
          };
          // We send the message
          request(gcmOptions, function (error, postRes, body){
            if (!error && postRes.statusCode == 200) {
                //var info = JSON.parse(body);
                console.log(" -> " +body);
                res.send(body);
            } else {
                console.log("error  -> " +error + " " + body);
                sendErrorResponse(500, 500, 'Error '+body, res);
            }
          });
      } else {
        // No data with the given address
        res.send({'message':'No username with the given name'});
      }
    }
  });
});


app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'))
})


